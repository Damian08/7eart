<?php

namespace App\Controller;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class HomeController extends AbstractController
{
	#[Route('/', name: 'home_index')]
	public function index(UserRepository $userRepository): Response
	{
		$users = $userRepository->findAll();

		
		return $this->render('home/index.html.twig', [
			'users' => $users,
		]);
	}

	#[Route('/app', name: 'home_app')]
	public function app(): Response
	{
		return $this->render('app.html.twig', [
			'controller_name' => 'HomeController',
		]);
	}
}
