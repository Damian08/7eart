<?php

namespace App\Controller\Api;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Attribute\Route;

class TestApi extends AbstractController
{
	#[Route('/api/test', name: 'testapi', methods: ['GET'])]
	public function test(): JsonResponse
	{
		return new JsonResponse([
			'message' => 'Hello World !',
			'path' => 'api/controlleur'
		]);
	}
}