<?php

namespace App\Controller\Api;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Attribute\Route;

class UsersMapInfos extends AbstractController
{
	#[Route('/api/usersmapinfos', name: 'api_get_usersmapinfos', methods: ['GET'])]
	public function index(UserRepository $repository): JsonResponse
	{
		$usersMapInfos = $repository->findUsersMapInfos();

		return new JsonResponse([
			'usersMapInfos' => $usersMapInfos
		], 200);
	}
}