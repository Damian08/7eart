<?php

namespace App\Controller;

use App\Entity\CategorieMetier;
use App\Form\CategorieMetierType;
use App\Repository\CategorieMetierRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

#[Route('/categorie')]
class CategorieMetierController extends AbstractController
{
    #[Route('/', name: 'app_categorie_metier_index', methods: ['GET'])]
    public function index(CategorieMetierRepository $categorieMetierRepository): Response
    {
        return $this->render('categorie_metier/index.html.twig', [
            'categorie_metiers' => $categorieMetierRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_categorie_metier_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $categorieMetier = new CategorieMetier();
        $form = $this->createForm(CategorieMetierType::class, $categorieMetier);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($categorieMetier);
            $entityManager->flush();

            return $this->redirectToRoute('app_categorie_metier_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('categorie_metier/new.html.twig', [
            'categorie_metier' => $categorieMetier,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_categorie_metier_show', methods: ['GET'])]
    public function show(CategorieMetier $categorieMetier): Response
    {
        return $this->render('categorie_metier/show.html.twig', [
            'categorie_metier' => $categorieMetier,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_categorie_metier_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, CategorieMetier $categorieMetier, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(CategorieMetierType::class, $categorieMetier);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_categorie_metier_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('categorie_metier/edit.html.twig', [
            'categorie_metier' => $categorieMetier,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_categorie_metier_delete', methods: ['POST'])]
    public function delete(Request $request, CategorieMetier $categorieMetier, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$categorieMetier->getId(), $request->request->get('_token'))) {
            $entityManager->remove($categorieMetier);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_categorie_metier_index', [], Response::HTTP_SEE_OTHER);
    }
}
