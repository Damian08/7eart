<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class UserFixture extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $user = new User();
        $user->setEmail('user@gmail.com')
            ->setNom('Wood')
            ->setPrenom('Chris')
            ->setPassword('password')
            ->setLatitude('48.862725')
            ->setLongitude('2.287592');

        $manager->persist($user);

        $user2 = new User();
        $user2->setEmail('user2@pm.me')
            ->setNom('Neill')
            ->setPrenom('Jack')
            ->setPassword('password')
            ->setLatitude('49.7729797363')
            ->setLongitude('4.706265926361');

        $manager->persist($user2);

        $manager->flush();
    }
}
