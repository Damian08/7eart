<?php

namespace App\DataFixtures;

use App\Entity\CategorieMetier;
use App\Entity\Metier;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class MetierFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $category1 = new CategorieMetier();
        $category1->setNom('Production Audiovisuelle');
        $manager->persist($category1);
        $this->addReference('categorie_production_audiovisuelle', $category1);

        $category2 = new CategorieMetier();
        $category2->setNom('Scénarisation');
        $manager->persist($category2);
        $this->addReference('categorie_scenarisation', $category2);

        $category3 = new CategorieMetier();
        $category3->setNom('Technique');
        $manager->persist($category3);
        $this->addReference('categorie_technique', $category3);

        $category4 = new CategorieMetier();
        $category4->setNom('Post-Production');
        $manager->persist($category4);
        $this->addReference('categorie_post_production', $category4);

        $category5 = new CategorieMetier();
        $category5->setNom('Distribution et Diffusion');
        $manager->persist($category5);
        $this->addReference('categorie_distribution_diffusion', $category5);

        $category6 = new CategorieMetier();
        $category6->setNom('Administration et Gestion');
        $manager->persist($category6);
        $this->addReference('categorie_administration_gestion', $category6);

        $category7 = new CategorieMetier();
        $category7->setNom('Formation et Recherche');
        $manager->persist($category7);
        $this->addReference('categorie_formation_recherche', $category7);

        $category8 = new CategorieMetier();
        $category8->setNom('Juridique et Réglementation');
        $manager->persist($category8);
        $this->addReference('categorie_juridique_reglementation', $category8);

        $category9 = new CategorieMetier();
        $category9->setNom('Marketing et Promotion');
        $manager->persist($category9);
        $this->addReference('categorie_marketing_promotion', $category9);

        $category10 = new CategorieMetier();
        $category10->setNom('Animation');
        $manager->persist($category10);
        $this->addReference('categorie_animation', $category10);
        
        // Métiers de la catégorie "Production Audiovisuelle"
        $realisateur = new Metier();
        $realisateur->setNom('Réalisateur');
        $realisateur->setCouleur('FF5733');
        $realisateur->setCategorie($category1);
        $manager->persist($realisateur);

        $producteur = new Metier();
        $producteur->setNom('Producteur');
        $producteur->setCouleur('FFBD33');
        $producteur->setCategorie($category1);
        $manager->persist($producteur);

        $assistantProduction = new Metier();
        $assistantProduction->setNom('Assistant de production');
        $assistantProduction->setCouleur('FF8C33');
        $assistantProduction->setCategorie($category1);
        $manager->persist($assistantProduction);

        $scenariste = new Metier();
        $scenariste->setNom('Scénariste');
        $scenariste->setCouleur('3366FF');
        $scenariste->setCategorie($category2);
        $manager->persist($scenariste);

        $scripte = new Metier();
        $scripte->setNom('Scripte');
        $scripte->setCouleur('339FFF');
        $scripte->setCategorie($category2);
        $manager->persist($scripte);

        $storyboarder = new Metier();
        $storyboarder->setNom('Storyboarder');
        $storyboarder->setCouleur('33CCFF');
        $storyboarder->setCategorie($category2);
        $manager->persist($storyboarder);

        // Métiers de la catégorie "Technique"
        $ingenieurSon = new Metier();
        $ingenieurSon->setNom('Ingénieur du son');
        $ingenieurSon->setCouleur('33FFB2');
        $ingenieurSon->setCategorie($category3);
        $manager->persist($ingenieurSon);

        $monteurVideo = new Metier();
        $monteurVideo->setNom('Monteur vidéo');
        $monteurVideo->setCouleur('33FF66');
        $monteurVideo->setCategorie($category3);
        $manager->persist($monteurVideo);

        $directeurPhoto = new Metier();
        $directeurPhoto->setNom('Directeur de la photographie');
        $directeurPhoto->setCouleur('33FF33');
        $directeurPhoto->setCategorie($category3);
        $manager->persist($directeurPhoto);

        // Métiers de la catégorie "Post-Production"
        $monteurSon = new Metier();
        $monteurSon->setNom('Monteur son');
        $monteurSon->setCouleur('8C33FF');
        $monteurSon->setCategorie($category4);
        $manager->persist($monteurSon);

        $infographiste = new Metier();
        $infographiste->setNom('Infographiste');
        $infographiste->setCouleur('BD33FF');
        $infographiste->setCategorie($category4);
        $manager->persist($infographiste);

        $coloriste = new Metier();
        $coloriste->setNom('Coloriste');
        $coloriste->setCouleur('FF33E6');
        $coloriste->setCategorie($category4);
        $manager->persist($coloriste);

        // Métiers de la catégorie "Distribution et Diffusion"
        $responsableDistribution = new Metier();
        $responsableDistribution->setNom('Responsable de distribution');
        $responsableDistribution->setCouleur('FF3333');
        $responsableDistribution->setCategorie($category5);
        $manager->persist($responsableDistribution);

        $attachePresse = new Metier();
        $attachePresse->setNom('Attaché de presse');
        $attachePresse->setCouleur('FF335E');
        $attachePresse->setCategorie($category5);
        $manager->persist($attachePresse);

        $agentArtistique = new Metier();
        $agentArtistique->setNom('Agent artistique');
        $agentArtistique->setCouleur('FF338C');
        $agentArtistique->setCategorie($category5);
        $manager->persist($agentArtistique);

        // Métiers de la catégorie "Administration et Gestion"
        $directeurProd = new Metier();
        $directeurProd->setNom('Directeur de production');
        $directeurProd->setCouleur('33FFA6');
        $directeurProd->setCategorie($category6);
        $manager->persist($directeurProd);

        $adminProd = new Metier();
        $adminProd->setNom('Administrateur de production');
        $adminProd->setCouleur('33FF80');
        $adminProd->setCategorie($category6);
        $manager->persist($adminProd);

        $chargeProjet = new Metier();
        $chargeProjet->setNom('Chargé de projet audiovisuel');
        $chargeProjet->setCouleur('33FF4D');
        $chargeProjet->setCategorie($category6);
        $manager->persist($chargeProjet);

        // Métiers de la catégorie "Formation et Recherche"
        $enseignantAV = new Metier();
        $enseignantAV->setNom('Enseignant en audiovisuel');
        $enseignantAV->setCouleur('FFA633');
        $enseignantAV->setCategorie($category7);
        $manager->persist($enseignantAV);

        $chercheurMedias = new Metier();
        $chercheurMedias->setNom('Chercheur en médias');
        $chercheurMedias->setCouleur('FFA633');
        $chercheurMedias->setCategorie($category7);
        $manager->persist($chercheurMedias);

        $consultantAV = new Metier();
        $consultantAV->setNom('Consultant en audiovisuel');
        $consultantAV->setCouleur('FF8C33');
        $consultantAV->setCategorie($category7);
        $manager->persist($consultantAV);

        // Métiers de la catégorie "Juridique et Réglementation"
        $avocatAudiovisuel = new Metier();
        $avocatAudiovisuel->setNom('Avocat spécialisé en droit de l\'audiovisuel');
        $avocatAudiovisuel->setCouleur('FF5733');
        $avocatAudiovisuel->setCategorie($category8);
        $manager->persist($avocatAudiovisuel);

        $juristeMedias = new Metier();
        $juristeMedias->setNom('Juriste en médias');
        $juristeMedias->setCouleur('FF5733');
        $juristeMedias->setCategorie($category8);
        $manager->persist($juristeMedias);

        $responsableDroitsAuteur = new Metier();
        $responsableDroitsAuteur->setNom('Responsable des droits d\'auteur');
        $responsableDroitsAuteur->setCouleur('FF5733');
        $responsableDroitsAuteur->setCategorie($category8);
        $manager->persist($responsableDroitsAuteur);

        // Métiers de la catégorie "Marketing et Promotion"
        $responsableMarketing = new Metier();
        $responsableMarketing->setNom('Responsable marketing audiovisuel');
        $responsableMarketing->setCouleur('FF5733');
        $responsableMarketing->setCategorie($category9);
        $manager->persist($responsableMarketing);

        $attacheCommercial = new Metier();
        $attacheCommercial->setNom('Attaché commercial en audiovisuel');
        $attacheCommercial->setCouleur('FF5733');
        $attacheCommercial->setCategorie($category9);
        $manager->persist($attacheCommercial);

        $publicitaireAV = new Metier();
        $publicitaireAV->setNom('Publicitaire spécialisé en audiovisuel');
        $publicitaireAV->setCouleur('FF5733');
        $publicitaireAV->setCategorie($category9);
        $manager->persist($publicitaireAV);

        // Métiers de la catégorie "Animation"
        $animateur2D3D = new Metier();
        $animateur2D3D->setNom('Animateur 2D/3D');
        $animateur2D3D->setCouleur('33B2FF');
        $animateur2D3D->setCategorie($category10);
        $manager->persist($animateur2D3D);

        $realisateurAnimation = new Metier();
        $realisateurAnimation->setNom('Réalisateur d\'animation');
        $realisateurAnimation->setCouleur('33A6FF');
        $realisateurAnimation->setCategorie($category10);
        $manager->persist($realisateurAnimation);
        
        $characterDesigner = new Metier();
        $characterDesigner->setNom('Character Designer');
        $characterDesigner->setCouleur('3399FF');
        $characterDesigner->setCategorie($category10);
        $manager->persist($characterDesigner);

        $manager->flush();
    }
}
